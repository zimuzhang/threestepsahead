﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EUpgradeType
{
    Card, Buff
}

[System.Serializable]
public class Upgrade
{
    public EUpgradeType type;
    public string name;
}

[System.Serializable]
public class UpgradeChoices
{
    public List<Upgrade> choices;
}

public class CharacterUpgrades : MonoBehaviour
{
    public List<UpgradeChoices> upgrades = new List<UpgradeChoices>();
}
