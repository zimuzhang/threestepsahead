﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandImageDisplay : MonoBehaviour
{
    [SerializeField] private Text cardName = null;
    [SerializeField] private Image sprite = null;
    [SerializeField] private Text cardText = null;
    [SerializeField] private Image map = null;
    
    public void UpdateDisplay(CardData card)
    {
        cardName.text = card.name;
        sprite.sprite = card.sprite;
        cardText.text = card.text;
        map.sprite = card.map;
    }

    public void RotateMap(Direction direction)
    {
        switch (direction)
        {
            case Direction.r:
                map.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                break;
            case Direction.d:
                map.transform.rotation = Quaternion.Euler(0.0f, 0.0f, -90.0f);
                break;
            case Direction.l:
                map.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 180.0f);
                break;
            case Direction.u:
                map.transform.rotation = Quaternion.Euler(0.0f, 0.0f, -270.0f);
                break;
        }
    }
}
