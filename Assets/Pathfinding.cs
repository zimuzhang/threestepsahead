﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : Singleton<Pathfinding>
{
    public Coord mapSize = new Coord();
    public int[,] map;

    // Returns null if unable to find path
    // I choose Dijkstra because heuristic is constant
    public List<Coord> FindPath(Coord start, Coord end)
    {
        List<PathNode> open = new List<PathNode>();
        open.Add(new PathNode(new Coord(start), null, 0));
        List<PathNode> close = new List<PathNode>();
        PathNode current = new PathNode(new Coord(-1, -1), null, 0);
        List<Coord> eswn = new List<Coord> { new Coord(1, 0), new Coord(0, -1), new Coord(-1, 0), new Coord(0, 1) };

        while (current.coord != end)
        {
            if (open.Count == 0)
            {
                return null;
            }
            current = open[0];
            open.RemoveAt(0);
            for (int i = 0; i < eswn.Count; ++i)
            {
                Coord newCoord = current.coord + eswn[i];
                if (0 <= newCoord.x && newCoord.x < mapSize.x && 0 <= newCoord.y && newCoord.y < mapSize.y && map[newCoord.x, newCoord.y] == 1)
                {
                    bool exist = false;
                    for (int j = 0; j < open.Count; ++j)
                    {
                        if (open[j].coord == newCoord)
                        {
                            exist = true;
                            if (open[j].cost < current.cost + 1)
                            {
                                break;
                            }
                            else
                            {
                                open[j].cost = current.cost + 1;
                                open[j].parent = current;
                            }
                        }
                    }
                    for (int j = 0; j < close.Count; ++j)
                    {
                        if (close[j].coord == newCoord)
                        {
                            exist = true;
                            if (close[j].cost < current.cost + 1)
                            {
                                break;
                            }
                            else
                            {
                                close[j].cost = current.cost + 1;
                                close[j].parent = current;
                            }
                        }
                    }
                    if (!exist)
                    {
                        open.Add(new PathNode(newCoord, current, current.cost + 1));
                    }
                }
            }
            open.Sort();
            close.Add(current);
        }
        List<Coord> path = new List<Coord>();
        do
        {
            path.Add(current.coord);
            current = current.parent;
        }
        while (current != null);
        path.Reverse();
        return path;
    }
}
