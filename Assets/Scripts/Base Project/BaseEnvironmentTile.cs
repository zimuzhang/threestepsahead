﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnvironmentTile : MonoBehaviour
{
    public List<BaseEnvironmentTile> Connections { get; set; }
    public BaseEnvironmentTile Parent { get; set; }
    public Vector3 Position { get; set; }
    public float Global { get; set; }
    public float Local { get; set; }
    public bool Visited { get; set; }
    public bool IsAccessible { get; set; }
    public GameObject item;
}