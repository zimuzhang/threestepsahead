﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BaseGame : MonoBehaviour
{
    [SerializeField] private Camera MainCamera;
    [SerializeField] private BaseCharacter Character;
    [SerializeField] private Canvas Menu;
    [SerializeField] private Canvas Hud;
    [SerializeField] private Transform CharacterStart;

    private RaycastHit[] mRaycastHits;
    private BaseCharacter mCharacter;
    private BaseEnvironment mMap;
    private float cameraOffset;

    private readonly int NumberOfRaycastHits = 1;

    void Start()
    {
        mRaycastHits = new RaycastHit[NumberOfRaycastHits];
        mMap = GetComponentInChildren<BaseEnvironment>();
        mCharacter = Instantiate(Character, transform);
        ShowMenu(true);
        cameraOffset = MainCamera.transform.position.x - mCharacter.transform.position.x;
    }

    private void Update()
    {
        // Check to see if the player has clicked a tile and if they have, try to find a path to that 
        // tile. If we find a path then the character will move along it to the clicked tile. 
        if(Input.GetMouseButtonDown(0))
        {
            Ray screenClick = MainCamera.ScreenPointToRay(Input.mousePosition);
            int hits = Physics.RaycastNonAlloc(screenClick, mRaycastHits);
            if( hits > 0)
            {
                BaseEnvironmentTile tile = mRaycastHits[0].transform.GetComponent<BaseEnvironmentTile>();
                if (tile != null)
                {
                    List<BaseEnvironmentTile> route = mMap.Solve(mCharacter.CurrentPosition, tile);
                    mCharacter.GoTo(route);
                }
            }
        }

        // Camera Follow Character
        MainCamera.transform.position = new Vector3(cameraOffset + mCharacter.transform.position.x, MainCamera.transform.position.y, MainCamera.transform.position.z);
    }

    public void ShowMenu(bool show)
    {
        if (Menu != null && Hud != null)
        {
            Menu.enabled = show;
            Hud.enabled = !show;

            if( show )
            {
                mCharacter.transform.position = CharacterStart.position;
                mCharacter.transform.rotation = CharacterStart.rotation;
                mMap.CleanUpWorld();
            }
            else
            {
                mCharacter.transform.position = mMap.Start.Position;
                mCharacter.transform.rotation = Quaternion.identity;
                mCharacter.CurrentPosition = mMap.Start;
                MainCamera.transform.position = new Vector3(MainCamera.transform.position.x + mCharacter.transform.position.x, MainCamera.transform.position.y, MainCamera.transform.position.z);
                cameraOffset = MainCamera.transform.position.x - mCharacter.transform.position.x;
            }
        }
    }

    public void Generate()
    {
        mMap.GenerateWorld();
    }

    public void Exit()
    {
#if !UNITY_EDITOR
        Application.Quit();
#endif
    }
}