﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // Inspired by https://www.youtube.com/watch?v=MFQhpwc6cKE

    private Transform followTarget;
    [SerializeField] private float smoothRate = 0.0f;
    private float angle;
    private Vector3 velocity = Vector3.zero;
    private Vector3 originaloffset;
    private Vector3 finalOffset = Vector3.zero;
    [SerializeField] private float distance = 0.0f;
    [SerializeField] private float distanceRate = 0.0f;

    private void Update()
    {
        if (Input.mouseScrollDelta.y != 0)
        {
            distance -= Input.mouseScrollDelta.y * distanceRate;
            distance = Mathf.Min(70.0f, Mathf.Max(-40.0f, distance));
            finalOffset = new Vector3(0, -Mathf.Sin(angle) * distance, -Mathf.Cos(angle) * distance) + originaloffset;
        }
    }

    private void LateUpdate()
    {
        transform.position = Vector3.SmoothDamp(transform.position, followTarget.position + finalOffset, ref velocity, smoothRate, float.MaxValue);
    }

    public void Follow(Transform target)
    {
        followTarget = target;
        angle = -Mathf.Deg2Rad * transform.rotation.eulerAngles.x;
        originaloffset = new Vector3(followTarget.position.x, 39.0f, -18.0f) - followTarget.position;
        finalOffset = new Vector3(0, -Mathf.Sin(angle) * distance, -Mathf.Cos(angle) * distance) + originaloffset;
    }
}
