﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Buff
{
    public string name;
    public bool permanent;
    public int turnsLeft;
}

[System.Serializable]
public class TileAnimation
{
    public string animation;
    public Coord tile;

    public TileAnimation()
    {

    }

    public TileAnimation(TileAnimation copy)
    {
        animation = copy.animation;
        tile = new Coord(copy.tile);
    }
}

[System.Serializable]
public class Action
{
    public List<Buff> buffs = new List<Buff>();
    public string selfAnimation;

    public bool isMove = false;
    public Coord move;
    public float moveDuration;

    public bool isAttack = false;
    public int attackStrength;
    public List<Coord> attackTiles = new List<Coord>();
    public Dictionary<string, int> debuffs = new Dictionary<string, int>();
    public List<TileAnimation> tileAnimations = new List<TileAnimation>();

    public Action()
    {

    }

    public Action(Action copy)
    {
        buffs = copy.buffs;
        isMove = copy.isMove;
        move = new Coord(copy.move);
        moveDuration = copy.moveDuration;
        selfAnimation = copy.selfAnimation;
        isAttack = copy.isAttack;
        for (int i = 0; i < copy.attackTiles.Count; ++i)
        {
            attackTiles.Add(copy.attackTiles[i]);
        }
        attackStrength = copy.attackStrength;
        for (int i = 0; i < copy.tileAnimations.Count; ++i)
        {
            tileAnimations.Add(new TileAnimation(copy.tileAnimations[i]));
        }
    }

    public void Rotate(int[,] direction)
    {
        if (isMove)
        {
            move = Rotate(move, direction);
        }

        if (isAttack)
        {
            for (int i = 0; i < attackTiles.Count; ++i)
            {
                attackTiles[i] = Rotate(attackTiles[i], direction);
            }
            for (int i = 0; i < tileAnimations.Count; ++i)
            {
                tileAnimations[i].tile = Rotate(tileAnimations[i].tile, direction);
            }
        }
    }

    private Coord Rotate(Coord vec, int[,] direction)
    {
        return new Coord(direction[0, 0] * vec.x + direction[0, 1] * vec.y, direction[1, 0] * vec.x + direction[1, 1] * vec.y);
    }

    public void EditLocation(Coord characterLocation, Coord mapSize)
    {
        if (isMove)
        {
            if (InMap(move + characterLocation, mapSize))
            {
                move += characterLocation;
            }
            else
            {
                move = characterLocation;
            }
        }

        if (isAttack)
        {
            for (int i = attackTiles.Count - 1; i >= 0; --i)
            {
                if (InMap(attackTiles[i] + characterLocation, mapSize))
                {
                    attackTiles[i] += characterLocation;
                }
                else
                {
                    attackTiles.RemoveAt(i);
                }
            }
            for (int i = tileAnimations.Count - 1; i >= 0; --i)
            {
                if (InMap(tileAnimations[i].tile + characterLocation, mapSize))
                {
                    tileAnimations[i].tile += characterLocation;
                }
                else
                {
                    tileAnimations.RemoveAt(i);
                }
            }
        }
    }

    private bool InMap(Coord check, Coord mapSize)
    {
        return !(check.x < 0 || check.x >= mapSize.x || check.y < 0 || check.y >= mapSize.y);
    }
}

[System.Serializable]
public class CardData
{
    public string name;
    public string text;
    public Sprite sprite;
    public Sprite map;
    public List<Action> actions = new List<Action>();

    public CardData()
    {

    }

    public CardData(CardData copy)
    {
        name = copy.name;
        text = copy.text;
        sprite = copy.sprite;
        map = copy.map;
        for (int i = 0; i < copy.actions.Count; ++i)
        {
            actions.Add(new Action(copy.actions[i]));
        }
    }

    public void Rotate(int[,] direction)
    {
        for (int i = 0; i < actions.Count; ++i)
        {
            actions[i].Rotate(direction);
        }
    }

    public void Globalize(Coord characterLocation, Coord mapSize)
    {
        for (int i = 0; i < actions.Count; ++i)
        {
            actions[i].EditLocation(characterLocation, mapSize);
        }
    }
}