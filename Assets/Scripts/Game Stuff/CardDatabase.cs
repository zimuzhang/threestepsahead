﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FourDirectionCardData : IEnumerable
{
    public CardData right;
    public CardData down;
    public CardData left;
    public CardData up;

    public IEnumerator GetEnumerator()
    {
        yield return right;
        yield return down;
        yield return left;
        yield return up;
    }

    public CardData PickDirection(int direction)
    {
        switch (direction)
        {
            case 0:
                return right;
            case 1:
                return down;
            case 2:
                return left;
            case 3:
                return up;
        }
        throw new System.Exception("No such direction");
    }
}

public enum Direction
{
    r, d, l, u
}

public class CardDatabase : Singleton<CardDatabase>
{
    [SerializeField] private List<CardData> cardDatas = new List<CardData>();
    private Dictionary<string, FourDirectionCardData> cards = new Dictionary<string, FourDirectionCardData>();
    // Start is called before the first frame update
    void Start()
    {
        LoadCardData();
    }

    public CardData GetCard(string name, Direction direction)
    {
        switch (direction)
        {
            case Direction.r:
                return cards[name].right;
            case Direction.d:
                return cards[name].down;
            case Direction.l:
                return cards[name].left;
            case Direction.u:
                return cards[name].up;
        }
        return null;
    }

    public FourDirectionCardData GetFourDirectionCardData(string name)
    {
        return cards[name];
    }

    public Sprite GetSprite(string name)
    {
        return cards[name].right.sprite;
    }

    // turn card data inputed from inspector into dictionary, and populate rotated versions
    public void LoadCardData()
    {
        cards = new Dictionary<string, FourDirectionCardData>();
        FourDirectionCardData tempCDWD;
        int[,] rotate90  = new int[2, 2] { {  0,  1 }, { -1,  0 } };
        int[,] rotate180 = new int[2, 2] { { -1,  0 }, {  0, -1 } };
        int[,] rotate270 = new int[2, 2] { {  0, -1 }, {  1,  0 } };
        foreach (CardData card in cardDatas)
        {
            tempCDWD = new FourDirectionCardData();

            tempCDWD.right = new CardData(card);

            tempCDWD.down = new CardData(card);
            tempCDWD.down.Rotate(rotate90);

            tempCDWD.left = new CardData(card);
            tempCDWD.left.Rotate(rotate180);

            tempCDWD.up = new CardData(card);
            tempCDWD.up.Rotate(rotate270);

            cards.Add(card.name, tempCDWD);
        }
    }
}
