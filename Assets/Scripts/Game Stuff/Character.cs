﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Character : MonoBehaviour
{
    public int maxHealth;
    public int health;
    public List<Buff> buffs = new List<Buff>();
    [SerializeField] private Animator heartPrefab;
    private Vector3 heartOffset = new Vector3(1.5f, 0.7f, -1.5f);
    private List<Animator> hearts = new List<Animator>();
    public Coord coord;
    public Animator animator;
    public List<string> deck = new List<string>();
    public List<FourDirectionCardData> hand = new List<FourDirectionCardData>();
    public List<CardData> actionQueue = new List<CardData> { null, null, null };
    public List<SpriteRenderer> queueImages = new List<SpriteRenderer>();
    public List<SpriteRenderer> queueMaps = new List<SpriteRenderer>();
    public List<SpriteRenderer> queueBlinders = new List<SpriteRenderer>();
    [SerializeField] private GameObject queueSprites = null;
    public int actionQueueCount = 0;


    public void PickCards(Coord targetCoord)
    {
        Coord mapSize = Pathfinding.Instance.mapSize;
        List<Coord> path = Pathfinding.Instance.FindPath(coord, targetCoord);
        Coord newCoord = new Coord(coord);
        Coord right = new Coord(1, 0);
        Coord down = new Coord(0, -1);
        Coord left = new Coord(-1, 0);
        Coord up = new Coord(0, 1);
        // p for probability
        float[][] pMap = new float[mapSize.x][];
        for (int i = 0; i < mapSize.x; ++i)
        {
            pMap[i] = new float[mapSize.y];
        }
        pMap[targetCoord.x][targetCoord.y] = 1.0f;

        for (int i = 0; i < actionQueue.Count; ++i)
        {
            // Populate map of probability distribution of target
            pMap = PredictTargetCoord(pMap, path.Count);
            // Record value score for each card
            List<List<float>> scores = new List<List<float>>();
            float totalScore = 0.0f;
            // Decide how much score moving is worth, magic number adjustable, I picked ones that feel right
            scores.Add(new List<float>());
            float moveScore = Mathf.Max(0.2f, path.Count * 0.5f - 0.6f);
            float pFollowPath = Mathf.Max(0.25f, Mathf.Min(1.0f, path.Count * 0.4f - 0.5f));
            float scoreNotFollowPath = moveScore * (1.0f - pFollowPath) / 3.0f;
            int whichDir = 2;
            Coord nextStep = path[1] - newCoord;
            if (nextStep == right)
            {
                whichDir = 0;
            }
            else if (nextStep == down)
            {
                whichDir = 1;
            }
            else if (nextStep == left)
            {
                whichDir = 2;
            }
            else if (nextStep == up)
            {
                whichDir = 3;
            }
            scores[0].Add(scoreNotFollowPath);
            scores[0].Add(scoreNotFollowPath);
            scores[0].Add(scoreNotFollowPath);
            scores[0].Add(scoreNotFollowPath);
            scores[0][whichDir] = moveScore * pFollowPath;
            totalScore += moveScore;

            // Loop through all possible cards to pick, gain score from potentially hitting target
            for (int j = 1; j < hand.Count; ++j)
            {
                scores.Add(new List<float>());
                foreach(CardData cardData in hand[j])
                {
                    float score = 0;
                    for (int k = 0; k < cardData.actions.Count; k++)
                    {
                        if (cardData.actions[k].isAttack)
                        {
                            for (int t = 0; t < cardData.actions[k].attackTiles.Count; t++)
                            {
                                int tempX = cardData.actions[k].attackTiles[t].x + newCoord.x;
                                int tempY = cardData.actions[k].attackTiles[t].y + newCoord.y;
                                if (0 <= tempX && tempX < mapSize.x && 0 <= tempY && tempY < mapSize.y)
                                {
                                    score += pMap[tempX][tempY] * cardData.actions[k].attackStrength;
                                }
                            }
                        }
                    }
                    scores[j].Add(score);
                    totalScore += score;
                }
            }
            // Pick card based on desirable path and probability of hitting player
            float pick = Random.Range(0.0f, totalScore);
            bool picked = false;
            for (int j = 0; j < scores.Count; j++)
            {
                if (!picked)
                {
                    for (int k = 0; k < 4; k++)
                    {
                        pick -= scores[j][k];
                        if (pick < 0)
                        {
                            actionQueue[i] = hand[j].PickDirection(k);
                            queueImages[i].sprite = actionQueue[i].sprite;
                            queueMaps[i].sprite = actionQueue[i].map;
                            queueMaps[i].transform.rotation = Quaternion.Euler(queueMaps[i].transform.rotation.eulerAngles.x, queueMaps[i].transform.rotation.eulerAngles.y, k * -90.0f);
                            queueImages[i].gameObject.SetActive(true);
                            queueBlinders[i].enabled = true;
                            picked = true;
                            break;
                        }
                    }
                }
            }
        }
    }

    private float[][] PredictTargetCoord(float[][] pMap, int distance)
    {
        int[,] map = Pathfinding.Instance.map;
        Coord mapSize = Pathfinding.Instance.mapSize;
        // magic numbers in here are just ones that look right on the graph
        // the further away target is from you, the more likely for them to be moving and not attacking
        float pMove = Mathf.Min(1.0f, 0.13f * distance + 0.2f);
        float pStay = 1.0f - pMove;
        // flawed hypothesis of equal chance on moving each direction
        pMove /= 4.0f;

        float[][] newPMap = new float[mapSize.x][];
        for (int i = 0; i < mapSize.x; ++i)
        {
            newPMap[i] = new float[mapSize.y];
        }

        // Calculate the probability of landing in each tile after this move
        for (int i = 0; i < mapSize.x; ++i)
        {
            for (int j = 0; j < mapSize.y; ++j)
            {
                if (pMap[i][j] != 0)
                {
                    newPMap[i][j] += pMap[i][j] * pStay;
                    float moveChance = pMap[i][j] * pMove;
                    if (i != 0)
                    {
                        newPMap[i - 1][j] += moveChance;
                    }
                    if (i != mapSize.x - 1)
                    {
                        newPMap[i + 1][j] += moveChance;
                    }
                    if (j != 0)
                    {
                        newPMap[i][j - 1] += moveChance;
                    }
                    if (j != mapSize.y - 1)
                    {
                        newPMap[i][j + 1] += moveChance;
                    }
                }
            }
        }

        // probability map finished
        return newPMap;
    }

    // Find cards from database
    public void CopyDeckToHand()
    {
        hand = new List<FourDirectionCardData>();
        for (int i = 0; i < deck.Count; ++i)
        {
            hand.Add(CardDatabase.Instance.GetFourDirectionCardData(deck[i]));
        }
    }

    // Clear action queue and reset images
    public virtual void ClearQueue()
    {
        for (int i = 0; i < actionQueue.Count; ++i)
        {
            actionQueue[i] = null;
            queueImages[i].gameObject.SetActive(false);
        }
        actionQueueCount = 0;
    }

    public IEnumerator Move(Coord newCoord, Vector3 destination, float duration, string animation)
    {
        animator.SetTrigger(animation);
        yield return null;
        Vector3 originalP = transform.position;
        Vector3 p;
        float timer = 0.0f;

        while (Vector3.Distance(transform.position, destination) > 0.01f)
        {
            timer += Time.deltaTime;
            p = Vector3.Lerp(originalP, destination, timer / duration);
            transform.position = p;
            yield return null;
        }
        coord = new Coord(newCoord);
    }

    public int FindBuff(string name)
    {
        for (int i = 0; i < buffs.Count; ++i)
        {
            if (buffs[i].name == name)
            {
                return i;
            }
        }
        return -1;
    }

    public bool HaveBuff(string name)
    {
        return FindBuff(name) != -1;
    }

    public int DealDamage(int dmg)
    {
        if (HaveBuff("Powerful"))
        {
            ++dmg;
        }
        return dmg;
    }

    public void TakeDamage(int dmg)
    {
        if (HaveBuff("Frail"))
        {
            ++dmg;
        }
        if (HaveBuff("Guard"))
        {
            --dmg;
        }
        if (dmg > 0)
        {
            animator.SetTrigger("Hurt");
            int originalHealth = health;
            health = Mathf.Max(health - dmg, 0);
            for (int i = originalHealth - 1; i >= health; --i)
            {
                hearts[i].Play("EmptyHeart");
            }
        }
    }

    public void Heal(int heal)
    {
        int originalHealth = health;
        health = Mathf.Min(health + heal, maxHealth);
        for (int i = originalHealth - 1; i < health; i++)
        {
            hearts[i].Play("Heart");
        }
    }

    public IEnumerator Die()
    {
        queueSprites.SetActive(false);
        for (int i = 0; i < hearts.Count; i++)
        {
            hearts[i].gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }
        animator.SetTrigger("Die");
        yield return null;
        yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);
    }

    public void MakeHearts()
    {
        for (int i = 0; i < hearts.Count; i++)
        {
            Destroy(hearts[i].gameObject);
        }
        hearts = new List<Animator>();
        Animator tempHeart;
        for (int i = 0; i < maxHealth; i++)
        {
            tempHeart = Instantiate(heartPrefab, transform.position + new Vector3(heartOffset.x * i - 2.5f, heartOffset.y, heartOffset.z), transform.rotation);
            tempHeart.transform.SetParent(transform);
            hearts.Add(tempHeart);
        }
    }

    public void Buff(Buff buff)
    {
        int index = FindBuff(buff.name);
        if (index != -1)
        {
            buffs[index].permanent = buffs[index].permanent || buff.permanent;
            buffs[index].turnsLeft += buff.turnsLeft;
        }
        else
        {
            buffs.Add(buff);
        }
    }

    // Tick off each buff at the end of turn
    // Permanant buffs also get ticked but their turnsLeft never == 0 so never get removed
    public void BuffTick()
    {
        for (int i = buffs.Count - 1; i >= 0; --i)
        {
            --buffs[i].turnsLeft;
            if (buffs[i].turnsLeft == 0)
            {
                buffs.RemoveAt(i);
            }
        }
    }

    // This method is invoked from hand images when they're clicked
    public void CardClicked(int whichCard, Direction direction)
    {
        for (int i = 0; i < actionQueue.Count; ++i)
        {
            // find first empty spot in actionqueue and update it with clicked card
            if (actionQueue[i] == null)
            {
                float rotation = 0.0f;
                switch (direction)
                {
                    case Direction.r:
                        actionQueue[i] = hand[whichCard].right;
                        rotation = 0.0f;
                        break;
                    case Direction.d:
                        actionQueue[i] = hand[whichCard].down;
                        rotation = -90.0f;
                        break;
                    case Direction.l:
                        actionQueue[i] = hand[whichCard].left;
                        rotation = -180.0f;
                        break;
                    case Direction.u:
                        actionQueue[i] = hand[whichCard].up;
                        rotation = -270.0f;
                        break;
                }
                queueImages[i].sprite = actionQueue[i].sprite;
                queueMaps[i].sprite = actionQueue[i].map;
                queueMaps[i].transform.rotation = Quaternion.Euler(queueMaps[i].transform.rotation.eulerAngles.x, queueMaps[i].transform.rotation.eulerAngles.y, rotation);
                queueImages[i].gameObject.SetActive(true);
                ++actionQueueCount;
                break;
            }
        }
    }
}
