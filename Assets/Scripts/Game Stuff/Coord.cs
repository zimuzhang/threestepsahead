﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Coord
{
    public int x;
    public int y;

    public Coord(int i, int j)
    {
        x = i;
        y = j;
    }

    public Coord(Coord copy)
    {
        x = copy.x;
        y = copy.y;
    }

    public static Coord operator +(Coord coord1, Coord coord2)
    {
        return new Coord(coord1.x + coord2.x, coord1.y + coord2.y);
    }

    public static Coord operator -(Coord coord1, Coord coord2)
    {
        return new Coord(coord1.x - coord2.x, coord1.y - coord2.y);
    }

    public static bool operator ==(Coord coord1, Coord coord2)
    {
        return coord1.x == coord2.x && coord1.y == coord2.y;
    }

    public static bool operator !=(Coord coord1, Coord coord2)
    {
        return coord1.x != coord2.x || coord1.y != coord2.y;
    }

    public override bool Equals(object obj)
    {
        return obj is Coord coord &&
               x == coord.x &&
               y == coord.y;
    }

    public override int GetHashCode()
    {
        var hashCode = 1502939027;
        hashCode = hashCode * -1521134295 + x.GetHashCode();
        hashCode = hashCode * -1521134295 + y.GetHashCode();
        return hashCode;
    }
}