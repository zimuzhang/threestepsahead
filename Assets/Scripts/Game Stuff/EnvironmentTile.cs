﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentTile : MonoBehaviour
{
    public Vector2Int coord;
    public bool IsAccessible;
    public GameObject item;
    public Animator animator;
    public Renderer tileHighlight;
}
