﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    // Map and tile
    [SerializeField] private List<EnvironmentTile> accessibleTiles = new List<EnvironmentTile>();
    [SerializeField] private List<EnvironmentTile> inaccessibleTiles = new List<EnvironmentTile>();
    private EnvironmentTile[][] tiles = new EnvironmentTile[0][];
    [SerializeField] TileHighlight highlight = null;
    Vector3 tileSize = new Vector3(10.0f, 2.5f, 10.0f);
    private Vector3 tileCenterOffset = new Vector3(5.0f, 2.5f, 3.0f);

    // Cards
    [SerializeField] private Sprite noCardSprite = null;

    // Camera
    [SerializeField] private CameraFollow mainCamera = null;
    private Vector3 cameraAngle = new Vector3(46.0f, 0.0f, 0.0f);

    // Characters
    private List<Character> characters = new List<Character>();
    private PlayerCharacter playerCharacter;

    private List<GameObject> junk = new List<GameObject>();

    //UI Stuff
    [SerializeField] private Canvas upgradeCanvas = null;
    [SerializeField] private UpgradeBoard upgradeBoard = null;
    [SerializeField] private Canvas victoryCanvas = null;
    [SerializeField] private Image victorySprite = null;
    [SerializeField] private Canvas lossCanvas = null;
    [SerializeField] private Image lossSprite = null;


    // Start is called before the first frame update
    void Start()
    {
        // Make Playercharacter according to levelinfo
        Character character = Instantiate(LevelInformation.Instance.playerCharacter);
        character.CopyDeckToHand();
        character.MakeHearts();
        for (int i = 0; i < character.queueBlinders.Count; i++)
        {
            character.queueBlinders[i].sprite = null;
        }
        characters.Add(character);

        // Capture player script
        playerCharacter = characters[0].GetComponentInParent<PlayerCharacter>();
        playerCharacter.GetComponent<HandImageControl>().UpdateHandImage();
        victorySprite.sprite = playerCharacter.VictorySprite;
        lossSprite.sprite = playerCharacter.LossSprite;

        //
        victoryCanvas.gameObject.SetActive(false);
        lossCanvas.gameObject.SetActive(false);

        NextLevel();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ClearMap()
    {
        // destroy gameobjects
        for (int i = 0; i < tiles.Length; ++i)
        {
            for (int j = 0; j < tiles[i].Length; ++j)
            {
                Destroy(tiles[i][j].gameObject);
            }
        }
        // clear data
        tiles = new EnvironmentTile[0][];
    }

    void ClearCharacters()
    {
        // destroy gameobjects
        for (int i = characters.Count - 1; i >= 1; --i)
        {
            if (characters[i])
            {
                Destroy(characters[i].gameObject);
            }
            characters.RemoveAt(i);
        }
    }

    // access point for map generation button
    public void GenerateMapButton()
    {
        LevelInformation.Instance.currentLevel--;
        NextLevel();
    }


    public void ClearPlayerQueue()
    {
        if (characters.Count > 0)
        {
            characters[0].ClearQueue();
        }
    }

    public void ClearJunk()
    {
        for (int i = 0; i < junk.Count; ++i)
        {
            Destroy(junk[i]);
        }
        junk = new List<GameObject>();
    }

    private void StartLevelHideUI()
    {
        upgradeCanvas.gameObject.SetActive(false);
    }



    private void GenerateMap(LevelSeed level)
    {
        Coord mapSize = Pathfinding.Instance.mapSize = new Coord(level.mapSize);
        highlight.tiles = new Material[mapSize.x][];
        Pathfinding.Instance.map = new int[mapSize.x,mapSize.y];

        //Generate Map
        bool accessible;
        bool unreachable;
        do
        {
            // Generate a map
            for (int x = 0; x < mapSize.x; ++x)
            {
                for (int z = 0; z < mapSize.y; ++z)
                {
                    if (level.characterSpawnLoc.Contains(new Coord(x, z)))
                    {
                        accessible = true;
                    }
                    else
                    {
                        accessible = Random.Range(0.0f, 1.0f) < level.accessibleRate;
                    }

                    if (accessible)
                    {
                        Pathfinding.Instance.map[x, z] = 1;
                    }
                    else
                    {
                        Pathfinding.Instance.map[x, z] = 0;
                    }
                }
            }

            // Test if player character can reach all others
            unreachable = false;
            for (int i = 1; i < level.characterSpawnLoc.Count; i++)
            {
                List<Coord> path = Pathfinding.Instance.FindPath(level.characterSpawnLoc[0], level.characterSpawnLoc[i]);
                if (path == null)
                {
                    unreachable = true;
                    break;
                }
            }
        }
        while (unreachable);

        //Generate tiles according to map
        tiles = new EnvironmentTile[mapSize.x][];
        EnvironmentTile newTile;
        for (int x = 0; x < mapSize.x; ++x)
        {
            tiles[x] = new EnvironmentTile[mapSize.y];
            highlight.tiles[x] = new Material[mapSize.y];
            for (int z = 0; z < mapSize.y; ++z)
            {
                if (Pathfinding.Instance.map[x, z] == 1)
                {
                    newTile = accessibleTiles[Random.Range(0, accessibleTiles.Count)];
                }
                else
                {
                    newTile = inaccessibleTiles[Random.Range(0, inaccessibleTiles.Count)];
                }

                newTile = Instantiate(newTile, new Vector3(x * tileSize.x, 0.0f, z * tileSize.z), Quaternion.identity);
                newTile.coord = new Vector2Int(x, z);
                newTile.gameObject.name = string.Format("Tile({0},{1})", x, z);
                highlight.tiles[x][z] = newTile.tileHighlight.materials[1];
                tiles[x][z] = newTile;
            }
        }
        highlight.LogColors();

        // Instantiate Characters
        Character character;
        for (int i = 1; i < level.characters.Count; ++i)
        {
            character = Instantiate(level.characters[i], CoordToPosition(level.characterSpawnLoc[i]), Quaternion.Euler(cameraAngle));
            character.coord = level.characterSpawnLoc[i];
            tiles[character.coord.x][character.coord.y].IsAccessible = false;
            character.CopyDeckToHand();
            character.MakeHearts();
            characters.Add(character);
        }

        // Adjust Player Character
        Character player = characters[0];
        player.coord = level.characterSpawnLoc[0];
        player.transform.position = CoordToPosition(player.coord);
        tiles[player.coord.x][player.coord.y].IsAccessible = false;
        mainCamera.enabled = true;
        mainCamera.Follow(playerCharacter.transform);

        PopulateDecisions();
    }


    public IEnumerator CharacterDoCard(int characterIndex, CardData inCard)
    {
        CardData card = new CardData(inCard);
        card.Globalize(characters[characterIndex].coord, Pathfinding.Instance.mapSize);
        Action tempAction;
        Coord tempTile;
        List<int> toStopAnimation = new List<int>();
        for (int i = 0; i < card.actions.Count; ++i)
        {
            tempAction = card.actions[i];
            // buffs
            foreach (Buff buff in tempAction.buffs)
            {
                characters[characterIndex].Buff(buff);
            }

            // animation
            if (tempAction.selfAnimation != "")
            {
                characters[characterIndex].animator.SetTrigger(tempAction.selfAnimation);
                toStopAnimation.Add(characterIndex);
                yield return null;
            }

            // attack
            if (tempAction.isAttack)
            {
                highlight.HighLightThese(tempAction.attackTiles);
                for (int j = 0; j < tempAction.tileAnimations.Count; ++j)
                {
                    tempTile = tempAction.tileAnimations[j].tile;
                    tiles[tempTile.x][tempTile.y].animator.SetTrigger(tempAction.tileAnimations[j].animation);
                }
                for (int j = 0; j < tempAction.attackTiles.Count; ++j)
                {
                    for (int k = 0; k < characters.Count; ++k)
                    {
                        if (characters[k] && characters[k].coord == tempAction.attackTiles[j])
                        {
                            characters[k].TakeDamage(characters[characterIndex].DealDamage(tempAction.attackStrength));
                            toStopAnimation.Add(k);
                        }
                    }
                }
                yield return null;
                yield return new WaitForSeconds(characters[characterIndex].animator.GetCurrentAnimatorStateInfo(0).length);
            }


            // movement
            if (tempAction.isMove)
            {
                // do the move
                if (tiles[tempAction.move.x][tempAction.move.y].IsAccessible)
                {
                    Coord oldCoord = characters[characterIndex].coord;
                    yield return characters[characterIndex].Move(tempAction.move, CoordToPosition(tempAction.move), tempAction.moveDuration, tempAction.selfAnimation);
                    toStopAnimation.Add(characterIndex);
                    tiles[oldCoord.x][oldCoord.y].IsAccessible = true;
                    tiles[tempAction.move.x][tempAction.move.y].IsAccessible = false;
                }
                yield return null;
            }

            highlight.RestoreColor();

            // Check and clear characters with 0 health
            for (int j = characters.Count - 1; j > 0; --j)
            {
                if (characters[j] && characters[j].health == 0)
                {
                    yield return characters[j].Die();
                    tiles[characters[j].coord.x][characters[j].coord.y].IsAccessible = true;
                    junk.Add(characters[j].gameObject);
                    characters[j] = null;
                }
            }

            //Check victory or lost conditions
            if (characters[0].health == 0)
            {
                yield return characters[0].Die();
                Lose();
            }
            else if (WinCondition())
            {
                Win();
            }
            else
            {
                for (int j = 0; j < toStopAnimation.Count; ++j)
                {
                    if (characters[toStopAnimation[j]])
                    {
                        characters[toStopAnimation[j]].animator.SetTrigger("Idle");
                    }
                }
                yield return null;
                toStopAnimation = new List<int>();
            }
        }
    }

    private bool WinCondition()
    {
        for (int i = 1; i < characters.Count; ++i)
        {
            if (characters[i] != null)
            {
                return false;
            }
        }
        return true;
    }

    private void Lose()
    {
        DisableInteraction();
        StopAllCoroutines();
        lossCanvas.gameObject.SetActive(true);
    }

    private void Win()
    {
        DisableInteraction();
        StopAllCoroutines();
        if (LevelInformation.Instance.currentLevel == LevelInformation.Instance.totalLevel)
        {
            victoryCanvas.gameObject.SetActive(true);
        }
        else
        {
            upgradeBoard.UpdateUpgrade(playerCharacter.GetComponent<CharacterUpgrades>().upgrades[LevelInformation.Instance.currentLevel]);
            upgradeCanvas.gameObject.SetActive(true);
        }
    }

    private void NextLevel()
    {
        ++LevelInformation.Instance.currentLevel;
        StartLevelHideUI();
        ClearPlayerQueue();
        ClearJunk();
        ClearMap();
        ClearCharacters();
        GenerateMap(LevelInformation.Instance.levels[LevelInformation.Instance.currentLevel]);
        characters[0].Heal(characters[0].maxHealth);
        for (int i = characters[0].buffs.Count - 1; i >= 0 ; --i)
        {
            if (!characters[0].buffs[i].permanent)
            {
                characters[0].buffs.RemoveAt(i);
            }
        }
    }

    public IEnumerator ExecuteCards()
    {
        if (characters[0].actionQueueCount == 3)
        {
            for (int i = 0; i < characters[0].actionQueue.Count; ++i)
            {
                for (int j = 0; j < characters.Count; ++j)
                {
                    if (characters[j])
                    {
                        characters[j].queueBlinders[i].enabled = false;
                        yield return CharacterDoCard(j, characters[j].actionQueue[i]);
                        characters[j].actionQueue[i] = null;
                        --characters[j].actionQueueCount;
                    }
                }
            }
            yield return EndOfTurn();
            yield return BeginOfTurn();
        }
    }

    private Vector3 CoordToPosition(Coord coord)
    {
        return new Vector3(coord.x * tileSize.x, 0.0f, coord.y * tileSize.z) + tileCenterOffset;
    }

    public void ExecuteCardButton()
    {
        StartCoroutine(ExecuteCards());
    }

    private void DisableInteraction()
    {
        playerCharacter.GetComponent<HandImageControl>().DisableInteraction();
    }

    private void EnableInteraction()
    {
        playerCharacter.GetComponent<HandImageControl>().EnableInteraction();
    }

    private IEnumerator BeginOfTurn()
    {
        for (int i = 0; i < characters.Count; i++)
        {
            for (int j = 0; j < characters[i].queueImages.Count; j++)
            {
                characters[i].queueImages[j].gameObject.SetActive(false);
            }
        }
        PopulateDecisions();
        return null;
    }

    private IEnumerator EndOfTurn()
    {
        for (int i = 0; i < characters.Count; i++)
        {
            if (characters[i])
            {
                characters[i].BuffTick();
            }
        }
        return null;
    }

    private void PopulateDecisions()
    {
        for (int i = 1; i < characters.Count; ++i)
        {
            if (characters[i])
            {
                characters[i].PickCards(characters[0].coord);
            }
        }
    }

    public void UpgradePicked(Upgrade upgrade)
    {
        switch (upgrade.type)
        {
            case EUpgradeType.Card:
                playerCharacter.UpgradeCard(upgrade.name);
                break;
            case EUpgradeType.Buff:
                playerCharacter.UpgradeBuff(upgrade.name);
                break;
        }
        EnableInteraction();
        NextLevel();
    }
}
