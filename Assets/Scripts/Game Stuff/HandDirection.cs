﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HandDirection : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler
{
    [SerializeField] private HandImage image = null;
    [SerializeField] private Direction direction = Direction.r;
    [SerializeField] private HandImageDisplay display = null;

    public void OnPointerClick(PointerEventData eventData)
    {
        image.CardClicked(direction);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        display.RotateMap(direction);
    }
}
