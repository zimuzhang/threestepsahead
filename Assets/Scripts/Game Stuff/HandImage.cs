﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HandImage : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private HandImageControl control;
    private Character character;
    private RectTransform rectTransform;
    [SerializeField] private GameObject directions = null;
    public int cardNo;
    private Vector2 position;
    private Vector2 upPosition;
    private Vector2 leftPosition;
    private Vector2 rightPosition;
    private Vector2 targetPosition;
    private Vector3 normalScale = new Vector3(1.5f, 1.5f, 1.5f);
    private Vector3 upScale = new Vector3(2.1f, 2.1f, 2.1f);
    private Vector3 targetScale = new Vector3();
    private float shuffleSpeed = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        targetScale = normalScale;
        control = GetComponentInParent<HandImageControl>();
        character = GetComponentInParent<Character>();
        rectTransform = GetComponent<RectTransform>();
    }

    public void UpdatePositions(Vector2 pos, Vector2 cardSize, float gap)
    {
        position = targetPosition = pos;
        upPosition = position + new Vector2(0.0f, cardSize.y * 0.6f);
        Vector2 shuffleDistance = new Vector2(Mathf.Max(cardSize.x + 60.0f - gap, 0.0f), 0.0f);
        leftPosition = position - shuffleDistance;
        rightPosition = position + shuffleDistance;
    }

    // Update is called once per frame
    void Update()
    {
        rectTransform.localScale = Vector3.Lerp(rectTransform.localScale, targetScale, shuffleSpeed);
        rectTransform.anchoredPosition = Vector2.Lerp(rectTransform.anchoredPosition, targetPosition, shuffleSpeed);
    }

    public void ToTheLeft()
    {
        targetPosition = leftPosition;
    }

    public void ToTheRight()
    {
        targetPosition = rightPosition;
    }

    public void ToCenter()
    {
        targetPosition = position;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        targetPosition = upPosition;
        control.CardShuffle(cardNo);
        targetScale = upScale;
        directions.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        control.CardShuffleBack();
        targetScale = normalScale;
        directions.SetActive(false);
    }

    public void CardClicked(Direction direction)
    {
        character.CardClicked(cardNo, direction);
    }
}
