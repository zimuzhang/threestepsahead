﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandImageControl : MonoBehaviour
{
    [SerializeField] private Character character = null;
    public int handCount;
    public List<HandImage> handImages;
    [SerializeField] private HandImage handPrefab = null;
    [SerializeField] private Canvas canvas = null;
    private Vector2 cardSize = new Vector2(280.0f, 200.0f);
    private float handY = -410.0f;
    private float handXGapMax = 220.0f;
    private float handSpaceRange = 1200.0f;
    private float handSpaceCenter = -100.0f;

    public void UpdateHandImage()
    {
        // All procedures has to do with changing number of handImages go through this
        // Obviously not optimal to redo whole thing every time an edit is made
        // Will update and add better methods when design settles

        // Clear hand images before updating
        if (handImages != null)
        {
            for (int i = 0; i < handImages.Count; ++i)
            {
                Destroy(handImages[i].gameObject);
            }
        }
        handImages = new List<HandImage>();

        // Calculate x position of the cards
        handCount = character.hand.Count;
        float step = cardSize.x + Mathf.Min(handXGapMax, (handSpaceRange - handCount * cardSize.x) / (handCount - 1));
        float x = handSpaceCenter - (handCount - 1) / 2.0f * step;

        HandImage tempHandImage;
        HandImageDisplay tempDisplay;
        Image tempImage;
        for (int i = 0; i < handCount; ++i)
        {
            tempHandImage = Instantiate(handPrefab);
            tempHandImage.cardNo = i;
            tempHandImage.UpdatePositions(new Vector2(x, handY), cardSize, step);
            tempDisplay = tempHandImage.GetComponent<HandImageDisplay>();
            tempDisplay.UpdateDisplay(character.hand[i].right);
            tempImage = tempHandImage.GetComponentInParent<Image>();
            tempImage.transform.SetParent(canvas.transform);
            tempImage.GetComponent<RectTransform>().anchoredPosition = new Vector2(x, handY);
            tempImage.name = "Hand" + i;
            handImages.Add(tempHandImage);
            x += step;
        }
    }


    public void CardShuffle(int center)
    {
        int i = 0;
        while (i < center)
        {
            handImages[i].ToTheLeft();
            ++i;
        }
        ++i;
        while (i < handCount)
        {
            handImages[i].ToTheRight();
            ++i;
        }
    }

    public void CardShuffleBack()
    {
        for (int i = 0; i < handCount; ++i)
        {
            handImages[i].ToCenter();
        }
    }

    public void DisableInteraction()
    {
        for (int i = 0; i < handCount; ++i)
        {
            handImages[i].enabled = false;
        }
    }

    public void EnableInteraction()
    {
        for (int i = 0; i < handCount; ++i)
        {
            handImages[i].enabled = true;
        }
    }
}
