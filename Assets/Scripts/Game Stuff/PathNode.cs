﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class PathNode : IComparable<PathNode>
{
    public Coord coord;
    public PathNode parent = null;
    public int cost;

    public PathNode(Coord incoord, PathNode inparent, int incost)
    {
        coord = incoord;
        parent = inparent;
        cost = incost;
    }

    public int CompareTo(PathNode other)
    {
        return cost.CompareTo(other.cost);
    }
}
