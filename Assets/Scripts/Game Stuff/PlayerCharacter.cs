﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class PlayerCharacter : MonoBehaviour
{
    [SerializeField] private Character character = null;
    [SerializeField] private HandImageControl handImageControl = null;
    public Sprite VictorySprite;
    public Sprite LossSprite;

    private void Awake()
    {
        character = GetComponent<Character>();
        handImageControl = GetComponent<HandImageControl>();
    }

    public void UpgradeBuff(string buffName)
    {
        Buff buff = new Buff();
        buff.name = buffName;
        buff.permanent = true;
        buff.turnsLeft = -1;
        character.Buff(buff);
    }

    public void UpgradeCard(string cardName)
    {
        character.deck.Add(cardName);
        character.CopyDeckToHand();
        handImageControl.UpdateHandImage();
        handImageControl.DisableInteraction();
    }
}