﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileHighlight : MonoBehaviour
{
    public Material[][] tiles;
    private List<Coord> toBeRestored = new List<Coord>();
    private Color[][] originalColor;
    public Color attackColor;

    // Generate a slightly off green for each tile, and log them in originalColor
    public void LogColors()
    {
        float colorVary = 0.1f;
        originalColor = new Color[tiles.Length][];
        for (int i = 0; i < tiles.Length; ++i)
        {
            originalColor[i] = new Color[tiles[i].Length];
            for (int j = 0; j < tiles[i].Length; ++j)
            {
                Color color = tiles[i][j].color;
                color = new Color(color.r + Random.Range(-colorVary, colorVary), color.g + Random.Range(-colorVary, colorVary), color.b + Random.Range(-colorVary, colorVary));
                tiles[i][j].color = color;
                originalColor[i][j] = color;
            }
        }
    }

    // Change color of listed tiles to attack/move and add them to toBeRestored list
    public void HighLightThese(List<Coord> list)
    {
        for (int i = 0; i < list.Count; ++i)
        {
            tiles[list[i].x][list[i].y].SetColor("_Color", attackColor);
        }
        toBeRestored.AddRange(list);
    }

    // Shift all listed coords before highlighting
    public void HighLightTheseShift(List<Coord> list, Coord shift)
    {
        for (int i = 0; i < list.Count; ++i)
        {
            list[i] += shift;
        }
        HighLightThese(list);
    }

    // Restore listed color to original
    public void RestoreColor()
    {
        for (int i = 0; i < toBeRestored.Count; ++i)
        {
            tiles[toBeRestored[i].x][toBeRestored[i].y].SetColor("_Color", originalColor[toBeRestored[i].x][toBeRestored[i].y]);
        }
        toBeRestored = new List<Coord>();
    }
}
