﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HidableUI : MonoBehaviour
{
    private RectTransform rectTransform;
    [SerializeField] private Vector2 centerPosition;
    [SerializeField] private Vector2 sidePosition;

    // Start is called before the first frame update
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }


    private IEnumerator SlideHere(Vector2 targetPosition, float lerpTime)
    {
        Vector2 originalPosition = rectTransform.anchoredPosition;
        float timer = 0.0f;
        while (timer < lerpTime)
        {
            timer += Time.deltaTime;
            rectTransform.anchoredPosition = Vector2.Lerp(originalPosition, targetPosition, timer / lerpTime);
            yield return null;
        }
    }

    public IEnumerator SlideCenter(float lerpTime)
    {
        yield return SlideHere(centerPosition, lerpTime);
    }

    public IEnumerator SlideSide(float lerpTime)
    {
        yield return SlideHere(sidePosition, lerpTime);
    }
}
