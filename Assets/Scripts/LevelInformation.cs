﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct LevelSeed
{
    public float accessibleRate;
    public Coord mapSize;
    public List<Character> characters;
    public List<Coord> characterSpawnLoc;
}

public class LevelInformation : Singleton<LevelInformation>
{
    [SerializeField] public int currentLevel = 0;
    [SerializeField] public int totalLevel = 0;
    [SerializeField] public List<LevelSeed> levels = new List<LevelSeed>();
    [SerializeField] private List<Character> enemies = new List<Character>();
    public Character playerCharacter;

    void Start()
    {
        //LevelSeed levelSeed = new LevelSeed();
        //levelSeed.accessibleRate = 0.8f;
        //levelSeed.mapSize = new Vector2Int(6, 3);
        //levelSeed.characters = new List<Character>();
        //levelSeed.characters.Add(null);
        //levelSeed.characters.Add(FindCharacterByName("AntiBrute"));
        //levelSeed.characterSpawnLoc = new List<Vector2Int>();
        //levelSeed.characterSpawnLoc.Add(new Vector2Int(1, 1));
        //levelSeed.characterSpawnLoc.Add(new Vector2Int(4, 1));
        //levels.Add(levelSeed);

        //levelSeed = new LevelSeed();
        //levelSeed.accessibleRate = 0.8f;
        //levelSeed.mapSize = new Vector2Int(6, 6);
        //levelSeed.characters = new List<Character>();
        //levelSeed.characters.Add(null);
        //levelSeed.characters.Add(FindCharacterByName("AntiBrute"));
        //levelSeed.characters.Add(FindCharacterByName("AntiBrute"));
        //levelSeed.characterSpawnLoc = new List<Vector2Int>();
        //levelSeed.characterSpawnLoc.Add(new Vector2Int(1, 1));
        //levelSeed.characterSpawnLoc.Add(new Vector2Int(4, 2));
        //levelSeed.characterSpawnLoc.Add(new Vector2Int(2, 4));
        //levels.Add(levelSeed);

        //levelSeed = new LevelSeed();
        //levelSeed.accessibleRate = 1.0f;
        //levelSeed.mapSize = new Vector2Int(3, 3);
        //levelSeed.characters = new List<Character>();
        //levelSeed.characters.Add(null);
        //levelSeed.characters.Add(FindCharacterByName("AntiBrute"));
        //levelSeed.characterSpawnLoc = new List<Vector2Int>();
        //levelSeed.characterSpawnLoc.Add(new Vector2Int(0, 1));
        //levelSeed.characterSpawnLoc.Add(new Vector2Int(2, 0));
        //levels.Add(levelSeed);

        //levelSeed = new LevelSeed();
        //levelSeed.accessibleRate = 0.8f;
        //levelSeed.mapSize = new Vector2Int(6, 6);
        //levelSeed.characters = new List<Character>();
        //levelSeed.characters.Add(null);
        //levelSeed.characters.Add(FindCharacterByName("AntiBrute"));
        //levelSeed.characterSpawnLoc = new List<Vector2Int>();
        //levelSeed.characterSpawnLoc.Add(new Vector2Int(1, 3));
        //levelSeed.characterSpawnLoc.Add(new Vector2Int(4, 3));
        //levels.Add(levelSeed);
    }


    public void ChooseCharacter(Character character)
    {
        playerCharacter = character;
        for (int i = 0; i < levels.Count; ++i)
        {
            levels[i].characters[0] = playerCharacter;
        }
    }

    private Character FindCharacterByName(string name)
    {
        for (int i = 0; i < enemies.Count; ++i)
        {
            if (enemies[i].name == name)
            {
                return enemies[i];
            }
        }
        Debug.Log("Can't find character " + name);
        return null;
    }

    public bool PassLevel()
    {
        ++currentLevel;
        return currentLevel >= totalLevel;
    }

    public bool GameBeat()
    {
        return currentLevel > totalLevel;
    }

    public LevelSeed GetCurrentLevel()
    {
        return levels[currentLevel];
    }
}
