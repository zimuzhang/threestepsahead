﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LightUpOnHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private Image image;
    private Color dim = new Color(0.34f, 0.34f, 0.34f);   // color when dim
    private float rate = 0.1f;  // time it takes to light up

    void Awake()
    {
        image = GetComponent<Image>();
        image.alphaHitTestMinimumThreshold = 1.0f;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        LightUp();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Dim();
    }

    public void Dim()
    {
        // Stop Lighting up
        StopAllCoroutines();
        image.color = dim;
    }

    public void LightUp()
    {
        StartCoroutine(ChangeAlphaTo(Color.white));
    }

    // Lerp from current color to target color in interval of rate
    public IEnumerator ChangeAlphaTo(Color target)
    {
        float timer = 0.0f;
        Color originalColor = image.color;
        while (timer < 1.0f)
        {
            timer += Time.deltaTime;
            image.color = Color.Lerp(originalColor, target, timer / rate);
            yield return null;
        }
    }
}
