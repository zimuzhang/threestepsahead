﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelectionMenuControl : MonoBehaviour
{
    private RectTransform rectTransform;
    private float lerpTime = 0.2f;
    private Vector2 centerPosition = Vector2.zero;
    private Vector2 sidePosition = new Vector2(330.0f, 107.0f);
    [SerializeField] private Image backArrow;

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    public IEnumerator SlideHere(Vector2 targetPosition)
    {
        Vector2 originalPosition = rectTransform.anchoredPosition;
        float timer = 0.0f;
        while (timer < lerpTime)
        {
            timer += Time.deltaTime;
            rectTransform.anchoredPosition = Vector2.Lerp(originalPosition, targetPosition, timer / lerpTime);
            yield return null;
        }
    }

    public IEnumerator SlideCenter()
    {
        yield return SlideHere(centerPosition);
        backArrow.enabled = true;
    }

    public IEnumerator SlideSide()
    {
        backArrow.enabled = false;
        yield return SlideHere(sidePosition);
    }
}
