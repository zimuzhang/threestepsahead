﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuButtons : MonoBehaviour
{
    [SerializeField] private HidableUI characterMenu;
    [SerializeField] private HidableUI textMenu;
    [SerializeField] private Image backArrow;
    [SerializeField] private LightUpOnHover[] lightups; // Everything that can be lighted up
    [SerializeField] private float lerpTime;

    private void Start()
    {
        // at the beginning everything are lighted up
        for (int i = 0; i < lightups.Length; ++i)
        {
            lightups[i].LightUp();
            lightups[i].enabled = false;
        }
    }

    // Return to front page
    public void BackButton()
    {
        backArrow.enabled = false;
        StartCoroutine(characterMenu.SlideSide(lerpTime));
        StartCoroutine(textMenu.SlideCenter(lerpTime));
        for (int i = 0; i < lightups.Length; ++i)
        {
            lightups[i].LightUp();
            lightups[i].enabled = false;
        }
    }

    // Go to character selection page
    public void PlayButton()
    {
        StartCoroutine(Play());
    }

    private IEnumerator Play()
    {
        StartCoroutine(characterMenu.SlideCenter(lerpTime));
        StartCoroutine(textMenu.SlideSide(lerpTime));
        for (int i = 0; i < lightups.Length; ++i)
        {
            lightups[i].enabled = true;
            lightups[i].Dim();
        }
        yield return new WaitForSeconds(lerpTime);
        backArrow.enabled = true;
    }

    // Exit Game
    public void ExitButton()
    {
        Application.Quit();
    }
}
