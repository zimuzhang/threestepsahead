﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PickYourFighter : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private Character whichCharacter;

    public void OnPointerClick(PointerEventData eventData)
    {
        LevelInformation.Instance.ChooseCharacter(whichCharacter);
        LevelInformation.Instance.currentLevel = -1;
        SceneManager.LoadScene(1);
    }
}
