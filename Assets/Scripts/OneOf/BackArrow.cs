﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BackArrow : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private MainMenuButtons menu = null;

    public void OnPointerClick(PointerEventData eventData)
    {
        menu.BackButton();
    }
}
