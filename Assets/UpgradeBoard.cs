﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeBoard : MonoBehaviour
{
    [SerializeField] private List<UpgradeDisplay> displays;

    public void UpdateUpgrade(UpgradeChoices upgrade)
    {
        for (int i = 0; i < displays.Count; ++i)
        {
            displays[i].UpdateUpgrade(upgrade.choices[i]);
        }
    }
}
