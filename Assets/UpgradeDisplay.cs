﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class UpgradeDisplay : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private GameManager logic = null;
    [SerializeField] private Image image = null;
    private Upgrade upgrade;

    private void Awake()
    {
        image = GetComponent<Image>();
    }

    public void UpdateUpgrade(Upgrade inUpgrade)
    {
        upgrade = inUpgrade;
        if (upgrade.type == EUpgradeType.Card)
        {
            image.sprite = Resources.Load<Sprite>("CardSprite_" + upgrade.name);
        }
        else if (upgrade.type == EUpgradeType.Buff)
        {
            image.sprite = Resources.Load<Sprite>("Upgrade_" + upgrade.name);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        logic.UpgradePicked(upgrade);
    }
}
